public class Entrenador extends Persona {
    private int nivel;

    public Entrenador(String nombreEquipo, String colorUniforme, String nombre, String apellido, int nivel) {
        super(nombreEquipo, colorUniforme, nombre, apellido);
        this.nivel = nivel;
    }


    public String entrenar() {
        return "";
    }

    @Override
    public String presentarseNoJugador() {
        return super.presentarseNoJugador() + "que lleva " + nivel + " años con el equipo";
    }
}
