public class Jugador extends Persona{

    private String dorsal;
    private int valorAccion;
    //getter & Setter
    public String getDorsal() {
        return dorsal;
    }

    public void setDorsal(String dorsal) {
        this.dorsal = dorsal;
    }

    public int getValorAccion() {
        return valorAccion;
    }

    public void setValorAccion(int valorAccion) {
        this.valorAccion = valorAccion;
    }



//Constructor
    public Jugador(String nombreEquipo, String colorUniforme, String nombre, String apellido, String altura, String edad, String dorsal, int valorAccion) {
        super(nombreEquipo, colorUniforme, nombre, apellido, altura, edad);
        this.dorsal = dorsal;
        this.valorAccion = valorAccion;
    }


    @Override
    public String presentarse() {
        return super.presentarse()+"Dorsal: " + dorsal+"\n";
    }

    public int jugada(){
return (int) (Math.random() * 6);
    }


}
