public class Main {
    public static void main(String[] args) {
        //Inicio Torneo

        //Se definen 8 equipos
        Equipo[] listaEquipos = new Equipo[8];
        listaEquipos[0] = new Equipo("Los tinto frios", "Vinotinto");
        listaEquipos[1] = new Equipo("Los Sayas", "Rojo");
        listaEquipos[2] = new Equipo("Las Farc", "Blanco");
        listaEquipos[3] = new Equipo("Duque tu papa", "Amarillo con rojo");
        listaEquipos[4] = new Equipo("Uninpayu", "Naranja");
        listaEquipos[5] = new Equipo("Team Morris", "Azul Claro");
        listaEquipos[6] = new Equipo("Alpinitos", "Purpura");
        listaEquipos[7] = new Equipo("Sparkis", "Arcoiris");


        //Se crean 2 volantes por equipo
        Volante[] volantesEquipos = new Volante[16];
        volantesEquipos[0] = new Volante(listaEquipos[0].getNombreEquipo(), listaEquipos[0].getColorUniforme(), "Camilo", "Zambrano", "1,31", "21", "10", 0, 70);
        volantesEquipos[1] = new Volante(listaEquipos[0].getNombreEquipo(), listaEquipos[0].getColorUniforme(), "Juan", "Cubides", "1,60", "29", "9", 1, 100);
        volantesEquipos[2] = new Volante(listaEquipos[1].getNombreEquipo(), listaEquipos[1].getColorUniforme(), "Andres", "Camargo", "1,76", "42", "10", 0, 70);
        volantesEquipos[3] = new Volante(listaEquipos[1].getNombreEquipo(), listaEquipos[1].getColorUniforme(), "Sebastian", "Naranjo", "1,24", "21", "9", 1, 100);
        volantesEquipos[4] = new Volante(listaEquipos[2].getNombreEquipo(), listaEquipos[2].getColorUniforme(), "Victor", "Santafe", "1,65", "34", "7", 0, 70);
        volantesEquipos[5] = new Volante(listaEquipos[2].getNombreEquipo(), listaEquipos[2].getColorUniforme(), "Cleier", "Mosquera", "1,87", "39", "4", 1, 100);
        volantesEquipos[6] = new Volante(listaEquipos[3].getNombreEquipo(), listaEquipos[3].getColorUniforme(), "Ancizar", "Gomez", "1,34", "23", "10", 0, 70);
        volantesEquipos[7] = new Volante(listaEquipos[3].getNombreEquipo(), listaEquipos[3].getColorUniforme(), "Frank", "Ceballos", "1,63", "39", "7", 1, 100);
        volantesEquipos[8] = new Volante(listaEquipos[4].getNombreEquipo(), listaEquipos[4].getColorUniforme(), "James", "Melo", "1,75", "23", "9", 0, 70);
        volantesEquipos[9] = new Volante(listaEquipos[4].getNombreEquipo(), listaEquipos[4].getColorUniforme(), "Jhon", "Perez", "1,72", "38", "7", 1, 100);
        volantesEquipos[10] = new Volante(listaEquipos[5].getNombreEquipo(), listaEquipos[5].getColorUniforme(), "Juan", "Santos", "1,32", "21", "0", 0, 70);
        volantesEquipos[11] = new Volante(listaEquipos[5].getNombreEquipo(), listaEquipos[5].getColorUniforme(), "Carlos", "Iglesias", "1,63", "29", "9", 1, 100);
        volantesEquipos[12] = new Volante(listaEquipos[6].getNombreEquipo(), listaEquipos[6].getColorUniforme(), "Santiago", "Morales", "1,32", "21", "0", 0, 70);
        volantesEquipos[13] = new Volante(listaEquipos[6].getNombreEquipo(), listaEquipos[6].getColorUniforme(), "Yerson", "Barrera", "1,63", "29", "9", 1, 100);
        volantesEquipos[14] = new Volante(listaEquipos[7].getNombreEquipo(), listaEquipos[7].getColorUniforme(), "Victor", "Londoño", "1,32", "21", "0", 0, 70);
        volantesEquipos[15] = new Volante(listaEquipos[7].getNombreEquipo(), listaEquipos[7].getColorUniforme(), "Cesar", "Sastoque", "1,63", "29", "9", 1, 100);

        //Se crean 2 defensas por equipo
        Defensa[] defensasEquipos = new Defensa[16];
        defensasEquipos[0] = new Defensa(listaEquipos[0].getNombreEquipo(), listaEquipos[0].getColorUniforme(), "Antonio", "Hernandez", "1,61", "12", "21", 0, 1);
        defensasEquipos[1] = new Defensa(listaEquipos[0].getNombreEquipo(), listaEquipos[0].getColorUniforme(), "Diego", "Rey", "1,61", "12", "21", 0, 1);
        defensasEquipos[2] = new Defensa(listaEquipos[1].getNombreEquipo(), listaEquipos[1].getColorUniforme(), "Estewil", "Abondano", "1,61", "12", "21", 0, 1);
        defensasEquipos[3] = new Defensa(listaEquipos[1].getNombreEquipo(), listaEquipos[1].getColorUniforme(), "Gabriel", "Carvajal", "1,61", "12", "21", 0, 1);
        defensasEquipos[4] = new Defensa(listaEquipos[2].getNombreEquipo(), listaEquipos[2].getColorUniforme(), "Fabian", "Vila", "1,61", "12", "21", 0, 1);
        defensasEquipos[5] = new Defensa(listaEquipos[2].getNombreEquipo(), listaEquipos[2].getColorUniforme(), "Gabriel", "Monroy", "1,61", "12", "21", 0, 1);
        defensasEquipos[6] = new Defensa(listaEquipos[3].getNombreEquipo(), listaEquipos[3].getColorUniforme(), "Hugo", "Cortes", "1,61", "12", "21", 0, 1);
        defensasEquipos[7] = new Defensa(listaEquipos[3].getNombreEquipo(), listaEquipos[3].getColorUniforme(), "Jorge", "Polo", "1,61", "12", "21", 0, 1);
        defensasEquipos[8] = new Defensa(listaEquipos[4].getNombreEquipo(), listaEquipos[4].getColorUniforme(), "Julian", "Mogollon", "1,61", "12", "21", 0, 1);
        defensasEquipos[9] = new Defensa(listaEquipos[4].getNombreEquipo(), listaEquipos[4].getColorUniforme(), "Juan", "Pintor", "1,61", "12", "21", 0, 1);
        defensasEquipos[10] = new Defensa(listaEquipos[5].getNombreEquipo(), listaEquipos[5].getColorUniforme(), "Leonardo", "Barher", "1,61", "12", "21", 0, 1);
        defensasEquipos[11] = new Defensa(listaEquipos[5].getNombreEquipo(), listaEquipos[5].getColorUniforme(), "Mario", "Castiblanco", "1,61", "12", "21", 0, 1);
        defensasEquipos[12] = new Defensa(listaEquipos[6].getNombreEquipo(), listaEquipos[6].getColorUniforme(), "Alejandro", "Galeano", "1,61", "12", "21", 0, 1);
        defensasEquipos[13] = new Defensa(listaEquipos[6].getNombreEquipo(), listaEquipos[6].getColorUniforme(), "Alvaro", "Uribe", "1,61", "12", "21", 0, 1);
        defensasEquipos[14] = new Defensa(listaEquipos[7].getNombreEquipo(), listaEquipos[7].getColorUniforme(), "Holman", "Morris", "1,61", "12", "21", 0, 1);
        defensasEquipos[15] = new Defensa(listaEquipos[7].getNombreEquipo(), listaEquipos[7].getColorUniforme(), "Enrique", "Peñaloza", "1,61", "12", "21", 0, 1);
        //Se crea 1 arquero por equipo
        Arquero[] arqueroEquipos = new Arquero[8];
        arqueroEquipos[0] = new Arquero(listaEquipos[0].getNombreEquipo(), listaEquipos[0].getColorUniforme(), "Ricardo", "Muñoz", "1,56", "12", "12", 0, 0);
        arqueroEquipos[1] = new Arquero(listaEquipos[1].getNombreEquipo(), listaEquipos[1].getColorUniforme(), "Jaime", "Dussan", "1,56", "12", "12", 0, 0);
        arqueroEquipos[2] = new Arquero(listaEquipos[2].getNombreEquipo(), listaEquipos[2].getColorUniforme(), "Miguel", "Gomez", "1,56", "12", "12", 0, 0);
        arqueroEquipos[3] = new Arquero(listaEquipos[3].getNombreEquipo(), listaEquipos[3].getColorUniforme(), "Alejandro", "Torres", "1,56", "12", "12", 0, 0);
        arqueroEquipos[4] = new Arquero(listaEquipos[4].getNombreEquipo(), listaEquipos[4].getColorUniforme(), "Luis", "Nieto", "1,56", "12", "12", 0, 0);
        arqueroEquipos[5] = new Arquero(listaEquipos[5].getNombreEquipo(), listaEquipos[5].getColorUniforme(), "Fernando", "Jimenez", "1,56", "12", "12", 0, 0);
        arqueroEquipos[6] = new Arquero(listaEquipos[6].getNombreEquipo(), listaEquipos[6].getColorUniforme(), "Sergio", "Cadavid", "1,56", "12", "12", 0, 0);
        arqueroEquipos[7] = new Arquero(listaEquipos[7].getNombreEquipo(), listaEquipos[7].getColorUniforme(), "Alexandro", "Pato", "1,56", "12", "12", 0, 0);

        //Se crea 1 hincha por equipo
        Hincha[] hinchaEquipos = new Hincha[8];
        hinchaEquipos[0] = new Hincha(listaEquipos[0].getNombreEquipo(), listaEquipos[0].getColorUniforme(), "Alejandro", "Orozco");
        hinchaEquipos[1] = new Hincha(listaEquipos[1].getNombreEquipo(), listaEquipos[1].getColorUniforme(), "Esteban", "Barjunch");
        hinchaEquipos[2] = new Hincha(listaEquipos[2].getNombreEquipo(), listaEquipos[2].getColorUniforme(), "Addrian", "Lanao");
        hinchaEquipos[3] = new Hincha(listaEquipos[3].getNombreEquipo(), listaEquipos[3].getColorUniforme(), "Andres", "Del rio");
        hinchaEquipos[4] = new Hincha(listaEquipos[4].getNombreEquipo(), listaEquipos[4].getColorUniforme(), "Angel", "Zuñiga");
        hinchaEquipos[5] = new Hincha(listaEquipos[5].getNombreEquipo(), listaEquipos[5].getColorUniforme(), "Armando", "Dueñas");
        hinchaEquipos[6] = new Hincha(listaEquipos[6].getNombreEquipo(), listaEquipos[6].getColorUniforme(), "Tony", "Patiño");
        hinchaEquipos[7] = new Hincha(listaEquipos[7].getNombreEquipo(), listaEquipos[7].getColorUniforme(), "Homero", "Simpson");

        //Se crea 1 Entrenador por equipo
        Entrenador[] entrenadorEquipos = new Entrenador[8];
        entrenadorEquipos[0] = new Entrenador(listaEquipos[0].getNombreEquipo(), listaEquipos[0].getColorUniforme(), "Pep", "Sierra", 2);
        entrenadorEquipos[1] = new Entrenador(listaEquipos[1].getNombreEquipo(), listaEquipos[1].getColorUniforme(), "Dairo", "Horta ", 2);
        entrenadorEquipos[2] = new Entrenador(listaEquipos[2].getNombreEquipo(), listaEquipos[2].getColorUniforme(), "Eduardo", "Villamil", 2);
        entrenadorEquipos[3] = new Entrenador(listaEquipos[3].getNombreEquipo(), listaEquipos[3].getColorUniforme(), "David", "Garcia", 2);
        entrenadorEquipos[4] = new Entrenador(listaEquipos[4].getNombreEquipo(), listaEquipos[4].getColorUniforme(), "Rene", "Perez", 2);
        entrenadorEquipos[5] = new Entrenador(listaEquipos[5].getNombreEquipo(), listaEquipos[5].getColorUniforme(), "Felipe", "Luna", 2);
        entrenadorEquipos[6] = new Entrenador(listaEquipos[6].getNombreEquipo(), listaEquipos[6].getColorUniforme(), "Tomas", "Santos", 2);
        entrenadorEquipos[7] = new Entrenador(listaEquipos[7].getNombreEquipo(), listaEquipos[7].getColorUniforme(), "Sebastian", "Rodriguez", 2);

        //Torneo
        TorneoRun torneo = new TorneoRun();

        System.out.println(torneo.partido(hinchaEquipos, volantesEquipos, defensasEquipos, arqueroEquipos, entrenadorEquipos, hinchaEquipos));


    }
}


