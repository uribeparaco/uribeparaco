public class Persona extends Equipo {

    private String nombre;
    private String apellido;
    private String altura;
    private String edad;

    public Persona(String nombreEquipo, String colorUniforme, String nombre, String apellido, String altura, String edad) {
        super(nombreEquipo, colorUniforme);
        this.nombre = nombre;
        this.apellido = apellido;
        this.altura = altura;
        this.edad = edad;
    }

    public Persona(String nombreEquipo, String colorUniforme, String nombre, String apellido) {
        super(nombreEquipo, colorUniforme);
        this.nombre = nombre;
        this.apellido = apellido;
    }

    public String presentarse() {
        String texto = "";
        texto = texto + "Nombres: " + nombre + " " + apellido + "\n" +
                "Estatura: " + altura + "\n" +
                "Edad: " + edad + "\n";

        return texto;
    }

    public String presentarseNoJugador() {
        String texto = "";
        texto = texto + nombre + " " + apellido + " ";

        return texto;
    }

    public void respirar() {
    }

    ;

    public void caminar() {
    }

    ;


}
