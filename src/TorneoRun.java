public class TorneoRun {

    public String partido(Equipo[] equipos, Volante[] volantes, Defensa[] defensas, Arquero[] arqueros, Entrenador[] entrenadores, Hincha[] hinchas) {

//PRESENTACION DE JUGADORES
        String text = "";
        int[] jugadasLocal = {0, 0, 0};//[TirosVolante,DefendidasDefensa,TapadasArquero]
        int[] jugadasVisitante = {0, 0, 0};//[TirosVolante,DefendidasDefensa,TapadasArquero];
        int numeroEncuentros = 1;
        int golesLocal;
        int golesVisitante;
        int[] equiposSemifinalistas = new int[4];
        int[] equiposFinalistas = new int[2];
        int campeon;
        //EQUIPO LOCAL
        for (int i = 0; i <= 7; i = i + 2) {
            text = text + "_________________________________________________________________________" + "\n"
                    + "_________________________________________________________________________" + "\n" +
                    "En este " + numeroEncuentros + " encuentro jugara " + equipos[i].toString() + " y " + equipos[i + 1].toString() + "\n"
                    + "El equipo " + equipos[i].getNombreEquipo() + " jugara con los siguientes jugadores:";
            //VOLANTES & DELANTEROS LOCAL
            for (int j = 0; j <= 15; j++) {
                //VOLANTES
                if (volantes[j].getNombreEquipo() == equipos[i].getNombreEquipo()) {
                    text = text + "\n" + "\n" + "Volante " + "\n" + volantes[j].presentarse();
                    jugadasLocal[0] = (jugadasLocal[0] + volantes[j].jugada()) / 2;
                }
                //DEFENSAS
                if (defensas[j].getNombreEquipo() == equipos[i].getNombreEquipo()) {
                    text = text + "\n" + "\n" + "Defensa" + "\n" + defensas[j].presentarse();
                    jugadasLocal[1] = (jugadasLocal[1] + defensas[j].jugada()) / 2;
                }
            }

            //ARQUERO, ENTRENADOR, HINCHA LOCAL
            for (int j = 0; j <= 7; j++) {
                //ARQUERO
                if (arqueros[j].getNombreEquipo() == equipos[i].getNombreEquipo()) {
                    text = text + "\n" + "\n" + "Arquero" + "\n" + arqueros[j].presentarse();
                    jugadasLocal[2] = (jugadasLocal[2] + arqueros[j].jugada()) / 2;
                }
                //ENTRENADOR
                if (entrenadores[j].getNombreEquipo() == equipos[i].getNombreEquipo()) {
                    text = text + "\n" + "\n" + "El equipo lo dirige " + entrenadores[j].presentarseNoJugador();
                }
                //HINCHA
                if (hinchas[j].getNombreEquipo() == equipos[i].getNombreEquipo()) {
                    text = text + "\n" + "\n" + "por otro lado su amado hincha " + hinchas[j].presentarseNoJugador() + " grita desde la tribuna" + hinchas[j].alentar();
                }
            }

            //VISITANTE
            text = text + "\n" + "VS VS VS VS VS VS VS VS VS VS VS VS VS VS VS VS VS VS " + "\n" + "\n" + "Y El equipo rival " + equipos[i + 1].getNombreEquipo() + " jugara con los siguientes jugadores:";

            //VOLANTES & DELANTEROS VISITANTE
            for (int j = 0; j <= 15; j++) {
                //VOLANTES
                if (volantes[j].getNombreEquipo() == equipos[i + 1].getNombreEquipo()) {
                    text = text + "\n" + "\n" + "Volante" + "\n" + volantes[j].presentarse();
                    jugadasVisitante[0] = (jugadasVisitante[0] + volantes[j].jugada()) / 2;
                }
                //DEFENSAS
                if (defensas[j].getNombreEquipo() == equipos[i + 1].getNombreEquipo()) {
                    text = text + "\n" + "\n" + "Defensa" + "\n" + defensas[j].presentarse();
                    jugadasVisitante[1] = (jugadasVisitante[1] + defensas[j].jugada()) / 2;
                }

            }

            //ARQUERO, ENTRENADOR, HINCHA VISITANTE
            for (int j = 0; j <= 7; j++) {
                //ARQUERO
                if (arqueros[j].getNombreEquipo() == equipos[i + 1].getNombreEquipo()) {
                    text = text + "\n" + "\n" + "Arquero" + "\n" + arqueros[j].presentarse();
                    jugadasVisitante[2] = (jugadasVisitante[2] + arqueros[j].jugada()) / 2;
                }
                //ENTRENADOR
                if (entrenadores[j].getNombreEquipo() == equipos[i + 1].getNombreEquipo()) {
                    text = text + "\n" + "\n" + "El equipo lo dirige " + entrenadores[j].presentarseNoJugador();
                }
                //HINCHA
                if (hinchas[j].getNombreEquipo() == equipos[i + 1].getNombreEquipo()) {
                    text = text + "\n" + "\n" + "Como olvidar su unico y fiel hincha " + hinchas[j].presentarseNoJugador() + "que dice desde el banquillo " + hinchas[j].alentar();
                }
            }
            text = text + "\n" + "Incia el partido.." + "\n" + "Ya transcurrieron 85 minutos del partido, restan 5 minutos pintuco" + "\n" + "finaliza el encuentro.." + "\n";
            //calculo de goles
            golesLocal = jugadasLocal[0] - jugadasVisitante[1] - jugadasVisitante[2];
            golesVisitante = jugadasVisitante[0] - jugadasLocal[1] - jugadasLocal[2];
            text = text + "\n" + "Marcador final: " + equipos[i].getNombreEquipo() + " " + golesLocal + " - " + golesVisitante + " " + equipos[i + 1].getNombreEquipo() + "\n";
            if (golesLocal > golesVisitante) {
                equiposSemifinalistas[numeroEncuentros - 1] = i;
            } else {
                equiposSemifinalistas[numeroEncuentros - 1] = (i + 1);
            }
            text = text + "Ganador ⭐ " + equipos[equiposSemifinalistas[numeroEncuentros - 1]].getNombreEquipo() + " ⭐ \n";
            numeroEncuentros++;
        }

        //SEMIFINALES GANADOR 1 VS GANADOR 4 || GANADOR 2 VS GANADOR 3
        jugadasLocal = new int[]{0, 0, 0};
        jugadasVisitante = new int[]{0, 0, 0};

        text = text + "\n" + "\n" + "SEMIFINAL SEMIFINAL SEMIFINAL SEMIFINAL SEMIFINAL SEMIFINAL SEMIFINAL SEMIFINAL SEMIFINAL SEMIFINAL SEMIFINAL"
                + "\n" + "SEMIFINAL SEMIFINAL SEMIFINAL SEMIFINAL SEMIFINAL SEMIFINAL SEMIFINAL SEMIFINAL SEMIFINAL SEMIFINAL SEMIFINAL"
                + "\n" + "\n" +
                "No habra cambios de jugadores en ninguno de los equipos semifinalistas, tambien informan que jugaran con la misma vestimenta que la ronda pasada.\nLos partidos semifinales se jugaran de la siguiente manera \n \n" +
                "1. " + equipos[equiposSemifinalistas[0]].getNombreEquipo() + " VS " + equipos[equiposSemifinalistas[3]].getNombreEquipo() +
                "\n2. " + equipos[equiposSemifinalistas[1]].getNombreEquipo() + " VS " + equipos[equiposSemifinalistas[2]].getNombreEquipo()
                //Semifinal  No 1
                + "\n\nInicia el primer encuentro semifinal..\nTranscurren ya 60 minutos del encuentro y los jugadores se notan muy cansados..\nQueda 1 minuto para finalizar el encuentro y encontrar nuestro primer finalista\nEl colegiado indica que este partido entre el equipo " + equipos[equiposSemifinalistas[0]].getNombreEquipo() + " y " + equipos[equiposSemifinalistas[3]].getNombreEquipo() + " ya es historiaaaaaaaaaa.";

        //VOLANTES Y DELANTEROS LOCALES Y VISITANTES
        for (int j = 0; j <= 15; j++) {
            //VOLANTES LOCAl
            if (volantes[j].getNombreEquipo() == equipos[equiposSemifinalistas[0]].getNombreEquipo()) {
                jugadasLocal[0] = (jugadasLocal[0] + volantes[j].jugada()) / 2;
            }
            //DEFENSAS local
            if (defensas[j].getNombreEquipo() == equipos[equiposSemifinalistas[0]].getNombreEquipo()) {
                jugadasLocal[1] = (jugadasLocal[1] + defensas[j].jugada()) / 2;
            }

            //VOLANTES VISITANTES
            if (volantes[j].getNombreEquipo() == equipos[equiposSemifinalistas[3]].getNombreEquipo()) {
                jugadasVisitante[0] = (jugadasVisitante[0] + volantes[j].jugada()) / 2;
            }
            //DEFENSAS
            if (defensas[j].getNombreEquipo() == equipos[equiposSemifinalistas[3]].getNombreEquipo()) {
                jugadasVisitante[1] = (jugadasVisitante[1] + defensas[j].jugada()) / 2;
            }

        }

        //ARQUEROS LOCALES Y VISITANTES
        for (int j = 0; j <= 7; j++) {
            //ARQUERO LOCAL
            if (arqueros[j].getNombreEquipo() == equipos[equiposSemifinalistas[0]].getNombreEquipo()) {
                jugadasLocal[2] = (jugadasLocal[2] + arqueros[j].jugada()) / 2;
            }

            //ARQUERO VISITANTE
            if (arqueros[j].getNombreEquipo() == equipos[equiposSemifinalistas[3]].getNombreEquipo()) {
                jugadasVisitante[2] = (jugadasVisitante[2] + arqueros[j].jugada()) / 2;
            }

        }

        //calculo de goles
        golesLocal = jugadasLocal[0] - jugadasVisitante[1] - jugadasVisitante[2];
        golesVisitante = jugadasVisitante[0] - jugadasLocal[1] - jugadasLocal[2];
        if (golesLocal > golesVisitante) {
            equiposFinalistas[0] = equiposSemifinalistas[0];
        } else {
            equiposFinalistas[0] = equiposSemifinalistas[3];
        }
        text = text + "\n" + "Resultado final: " + equipos[equiposSemifinalistas[0]].getNombreEquipo() + " " + golesLocal + "-" + golesVisitante + " " + equipos[equiposSemifinalistas[3]].getNombreEquipo() + "\n"
                + "Nuestro primer finalista " + equipos[equiposFinalistas[0]].getNombreEquipo() + " iran descansar para recuperarse. Ya estan saboreando el titulo.";

        jugadasLocal = new int[]{0, 0, 0};
        jugadasVisitante = new int[]{0, 0, 0};

        //Semifinal  No 2
        text = text + "\n\nInicia el ultimo encuentro de la etapa semifinal, en esta ocacion se enfrenta " + equipos[equiposSemifinalistas[1]].getNombreEquipo() + " contra " + equipos[equiposSemifinalistas[2]].getNombreEquipo() + "\nFinaliza el primer tiempo y no se ven acciones de gol por ningun lado\nEl arbitro auxiliar suma 5 minutos de adicion.\nEl juez levanta las manos y da por finalizado el encuentro.";

        //VOLANTES Y DELANTEROS LOCALES Y VISITANTES
        for (int j = 0; j <= 15; j++) {
            //VOLANTES LOCAl
            if (volantes[j].getNombreEquipo() == equipos[equiposSemifinalistas[1]].getNombreEquipo()) {
                jugadasLocal[0] = (jugadasLocal[0] + volantes[j].jugada()) / 2;
            }
            //DEFENSAS local
            if (defensas[j].getNombreEquipo() == equipos[equiposSemifinalistas[1]].getNombreEquipo()) {
                jugadasLocal[1] = (jugadasLocal[1] + defensas[j].jugada()) / 2;
            }

            //VOLANTES VISITANTES
            if (volantes[j].getNombreEquipo() == equipos[equiposSemifinalistas[2]].getNombreEquipo()) {
                jugadasVisitante[0] = (jugadasVisitante[0] + volantes[j].jugada()) / 2;
            }
            //DEFENSAS VISITANTE
            if (defensas[j].getNombreEquipo() == equipos[equiposSemifinalistas[2]].getNombreEquipo()) {
                jugadasVisitante[1] = (jugadasVisitante[1] + defensas[j].jugada()) / 2;
            }

        }

        //ARQUEROS LOCALES Y VISITANTES
        for (int j = 0; j <= 7; j++) {
            //ARQUERO LOCAL
            if (arqueros[j].getNombreEquipo() == equipos[equiposSemifinalistas[1]].getNombreEquipo()) {
                jugadasLocal[2] = (jugadasLocal[2] + arqueros[j].jugada()) / 2;
            }

            //ARQUERO VISITANTE
            if (arqueros[j].getNombreEquipo() == equipos[equiposSemifinalistas[2]].getNombreEquipo()) {
                jugadasVisitante[2] = (jugadasVisitante[2] + arqueros[j].jugada()) / 2;
            }

        }

        //calculo de goles
        golesLocal = jugadasLocal[0] - jugadasVisitante[1] - jugadasVisitante[2];
        golesVisitante = jugadasVisitante[0] - jugadasLocal[1] - jugadasLocal[2];
        if (golesLocal > golesVisitante) {
            equiposFinalistas[1] = equiposSemifinalistas[1];
        } else {
            equiposFinalistas[1] = equiposSemifinalistas[2];
        }
        text = text + "\n" + "Resultado final: " + equipos[equiposSemifinalistas[1]].getNombreEquipo() + " " + golesLocal + "-" + golesVisitante + " " + equipos[equiposSemifinalistas[2]].getNombreEquipo() + "\n"
                + "Nuestro segundo finalista " + equipos[equiposFinalistas[1]].getNombreEquipo() + " debera enfrentar a " + equipos[equiposFinalistas[0]].getNombreEquipo()+" y luchar por la gran copa.¿ Quien ganara?... No se despegue de este monitor..";
        //FINAL TORNEO :(

        jugadasLocal = new int[]{0, 0, 0};
        jugadasVisitante = new int[]{0, 0, 0};

        text = text + "\n" + "\n" + "FINAL FINAL FINAL FINAL FINAL FINAL FINAL FINAL FINAL FINAL FINAL FINAL FINAL FINAL FINAL FINAL"
                + "\n" + "SFINAL FINAL FINAL FINAL FINAL FINAL FINAL FINAL FINAL FINAL FINAL FINAL FINAL FINAL FINAL FINAL"
                + "\n" + "\n" + "Hoy conoceremos el gran ganador de este torneo. ambos equipos merecen ganar, pues han sufrido para llegar hasta este punto."+

                "\nLos equipos " + equipos[equiposFinalistas[0]].getNombreEquipo() + " y "+ equipos[equiposFinalistas[1]].getNombreEquipo() +  " se enfrentaran a muerte, los dos tienen la racha de 2 victorias consecutivas y ninguno esta dispuesto a rendirse, dejaran todo en la cancha";

        text = text + "\n\nTodos los jugadores salen a la cancha, estan cantando muy fuerte su himno...\nSe acomodan cada uno en sus posiciones...\nEl arbitro autoriza el primer patadon al balon\nComienza el encuentro...."+ "\nfaltan 5 minutos para finalizar el primer tiempo, los jugadores se notan energicos aun.\nInicia el segundo tiempo. Los equipos se ven agresivos, que instrucciones habran recibido de su entrenador...\nFaltan 2 minutos para que se termine todo. Ya se empieza a notar el desgaste de cada uno de los jugadores. \nSe acabo señoras y señores, el arbitro da el pitazo final del certamen";

        //VOLANTES Y DELANTEROS LOCALES Y VISITANTES
        for (int j = 0; j <= 15; j++) {
            //VOLANTES LOCAl
            if (volantes[j].getNombreEquipo() == equipos[equiposFinalistas[0]].getNombreEquipo()) {
                jugadasLocal[0] = (jugadasLocal[0] + volantes[j].jugada()) / 2;
            }
            //DEFENSAS local
            if (defensas[j].getNombreEquipo() == equipos[equiposFinalistas[0]].getNombreEquipo()) {
                jugadasLocal[1] = (jugadasLocal[1] + defensas[j].jugada()) / 2;
            }

            //VOLANTES VISITANTES
            if (volantes[j].getNombreEquipo() == equipos[equiposFinalistas[1]].getNombreEquipo()) {
                jugadasVisitante[0] = (jugadasVisitante[0] + volantes[j].jugada()) / 2;
            }
            //DEFENSAS VISITANTE
            if (defensas[j].getNombreEquipo() == equipos[equiposFinalistas[1]].getNombreEquipo()) {
                jugadasVisitante[1] = (jugadasVisitante[1] + defensas[j].jugada()) / 2;
            }

        }

        //ARQUEROS LOCALES Y VISITANTES
        for (int j = 0; j <= 7; j++) {
            //ARQUERO LOCAL
            if (arqueros[j].getNombreEquipo() == equipos[equiposFinalistas[0]].getNombreEquipo()) {
                jugadasLocal[2] = (jugadasLocal[2] + arqueros[j].jugada()) / 2;
            }

            //ARQUERO VISITANTE
            if (arqueros[j].getNombreEquipo() == equipos[equiposFinalistas[1]].getNombreEquipo()) {
                jugadasVisitante[2] = (jugadasVisitante[2] + arqueros[j].jugada()) / 2;
            }

        }

        //calculo de goles
        golesLocal = jugadasLocal[0] - jugadasVisitante[1] - jugadasVisitante[2];
        golesVisitante = jugadasVisitante[0] - jugadasLocal[1] - jugadasLocal[2];
        if (golesLocal > golesVisitante) {
            campeon=equiposFinalistas[0];
        } else {
            campeon=equiposFinalistas[1];
        }
        text = text + "\n" + "Resultado final: " + equipos[equiposFinalistas[0]].getNombreEquipo()  + golesLocal + "" + golesVisitante + " " + equipos[equiposFinalistas[1]].getNombreEquipo() + "\n"
                + "\nEl equipo ganador " + equipos[campeon].getNombreEquipo()+" recibira 15 mil millones de pesos para gastarse en lo que se le antoje."+
                "\n  ..|'''.|     |     '||    ||' '||''|.  '||''''|   ..|''||   '|.   '|' \n" +
                ".|'     '     |||     |||  |||   ||   ||  ||  .    .|'    ||   |'|   |  \n" +
                "||           |  ||    |'|..'||   ||...|'  ||''|    ||      ||  | '|. |  " + equipos[campeon].getNombreEquipo()+"\n"+
                "'|.      .  .''''|.   | '|' ||   ||       ||       '|.     ||  |   |||  \n" +
                " ''|....'  .|.  .||. .|. | .||. .||.     .||.....|  ''|...|'  .|.   '| ";
        ;

        return text;

    }


}


