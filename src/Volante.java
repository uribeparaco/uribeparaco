public class Volante extends Jugador {

    private int nivelPatada;

    public int getNivelPatada() {
        return nivelPatada;
    }

    public void setNivelPatada(int nivelPatada) {
        this.nivelPatada = nivelPatada;
    }

    public Volante(String nombreEquipo, String colorUniforme, String nombre, String apellido, String altura, String edad, String dorsal, int valorAccion, int nivelPatada) {
        super(nombreEquipo, colorUniforme, nombre, apellido, altura, edad, dorsal, valorAccion);
        this.nivelPatada = nivelPatada;
    }


    @Override
    public String presentarse() {
        return super.presentarse() + "Puntaje Disparo:" + nivelPatada;
    }

    @Override
    public int jugada() {
        return super.jugada() * 3;
    }
}
